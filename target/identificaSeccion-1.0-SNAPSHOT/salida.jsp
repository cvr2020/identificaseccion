<%-- 
    Document   : salida
    Created on : 29-03-2020, 19:48:21
    Author     : CARLOS VILLAR RIVERA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Resumen del registro</h1>

        <%
            String nombre = (String) request.getAttribute("nombre");
            String seccion = (String) request.getAttribute("seccion");
        %>

        <p> <b>El alumno  <%=nombre%>,  pertenece a la sección numero  <%=seccion%> </b>  </p>

        <p>  <a href="index.jsp"> <---- VOLVER </a> </p>


    </body>
</html>
